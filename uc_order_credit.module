<?php
/**
 * @file
 * Ubercart order credit module
 *
 * Allows customers to be issued store credit that can be used to 
 * purchase goods at a later date.
 */
 
/**
 * Implements hook_perm().
 */
function uc_order_credit_perm() {
  return array('issue credit');
}

/**
 * Implements hook_line_item().
 */
function uc_order_credit_line_item() {
  $items[] = array(
    'id' => 'store_credit',
    'title' => t('Issued Store Credit'),
    'weight' => 2,
    'stored' => true,
    'add_list' => user_access('issue credit'),
    'calculated' => true,
    'display_only' => false,
  );
  
  $items[] = array(
    'id' => 'used_store_credit',
    'title' => t('Store Credit'),
    'weight' => 1,
    'stored' => false,
    'add_list' => false,
    'calculated' => true,
    'display_only' => false,
    'callback' => 'uc_line_item_used_store_credit',
  );

  return $items;
}

/**
 * Implements hook_checkout_pane().
 */
function uc_order_credit_checkout_pane() {
  $panes[] = array(
    'id' => 'order-credit',
    'callback' => 'uc_checkout_pane_order_credit',
    'title' => t('Store Credit'),
    'desc' => t('Allow customer to see their store credit'),
    'weight' => 1,
    'process' => false,
    'collapsible' => false,
    'show' => array('view'),
  );
  
  return $panes;
}

/**
 * Implements hook_order().
 */
function uc_order_credit_order($op, &$arg1, $arg2) {
/* This is currently flawed in that if theoretically if someone goes back after reviewing an order and
 * changes the order value, the credit amount may have already been applied... Need to fix that somehow.
 */
  switch($op) {
    case 'save':
      if($arg1->order_status == 'in_checkout') {
        // check to see if the user has credit
        $credit_balance = uc_order_credit_balance($arg1->uid);
        if($credit_balance > 0) {
          // make sure a credit payment hasn't been applied to this order
          $query = db_query("SELECT order_id,comment FROM {uc_payment_receipts} WHERE order_id = %d AND comment LIKE '%%auto store credit%%'", $arg1->order_id);
          if(!db_result($query)) {
            $total = uc_order_get_total($arg1);
            if($total < $credit_balance)
              $credit_balance = $total;
              
            uc_payment_enter($arg1->order_id, 'other', $credit_balance, $arg1->uid, NULL, 'Auto Store Credit');
          }
        }
      }
    break;
  }
}

/**
 * Handle the "Store Credit" order pane.
 */
function uc_checkout_pane_order_credit($op, &$arg1, $arg2) {
  global $user;
  
  switch($op) {
    case 'view':
      $credit_balance = uc_order_credit_balance($user->uid);
      if($credit_balance > 0) {
        $description = t('You have a recorded credit balance of !bal! This credit will be applied to your current order.', array('!bal' => uc_currency_format($credit_balance)));
        return array('description' => $description);
      }
      break;
  }
}

/**
 * Handle the used store credit line item
 */
function uc_line_item_used_store_credit($op, $arg1) {
  global $user;

  switch($op) {
    case 'load':
      if($arg1->order_status == 'in_checkout') {
        $credit = uc_order_credit_balance($arg1->uid, $arg1->order_id);
        if($credit > 0) {
          $total = uc_order_get_total($arg1);
          if($arg1->order_total < $credit) 
            $credit = $total;
            
          $lines[] = array(
            'id' => 'used_store_credit',
            'title' => t('Store Credit'),
            'amount' =>  $credit * -1,
          );
          
          return $lines;
        }
      }
      return array();
      break;
     
    /* 
     * Need a better way of getting order total,
     * this doesn't work for now...
     */
    /*
    case 'cart-preview':
      $items = uc_cart_get_contents();
	    foreach ($items as $item) {
       	$total += ($item->price) * $item->qty;
      }
      
      $credit = uc_order_credit_balance($user->uid, $arg1->order_id);
      if($credit > $total) {
        $credit = $total;
      }
      $credit = $credit * -1;
      
      if($credit < 0)
        drupal_add_js("\$(document).ready( function() { set_line_item('used_store_credit', '". t('Store Credit') ."', ". $credit .", -10); } );", 'inline');
      
      break;
     */
  }
}

/**
 * Gets the balance of a users store credit account.
 *
 * Order ID can be specified if the current order needs to be ignored (during checkout
 * this is neccessary or else store credit could end up showing as $0 which is confusing)
 *
 * @param $user_id
 *   A user ID integer
 *
 * @param $order_id
 *   (OPTIONAL) An order ID to be excluded from used credit assessment.
 *
 * @return
 *   Amount of store credit currently available to the user in question.
 */
function uc_order_credit_balance($user_id, $order_id = false) {
  // Fetch amount of credit currently issued to user
  $query = db_query("SELECT SUM(li.amount) FROM {uc_order_line_items} AS li "
                   ."LEFT JOIN {uc_orders} AS o ON li.order_id = o.order_id WHERE li.type = '%s' AND o.uid = %d", 'store_credit', $user_id);
  $purchased = db_result($query);
  
  // Find the amount of credit used by the user
  $query = db_query("SELECT SUM(p.amount) FROM uc_payment_receipts AS p "
                   ."LEFT JOIN uc_orders AS o ON p.order_id = o.order_id WHERE o.uid = %d AND p.method = 'Other' "
                   ."AND UPPER(p.comment) LIKE UPPER('%%auto store credit%%') AND o.order_id != %d", $user_id, $order_id);
  $used = db_result($query);
  
  if(!$used) $used = 0;
  if(!$purchased) $purchased = 0;
  
  return ($purchased - $used);
}
